
Complet DNS

3105

  
  

ARHEL Sébastien

Sommaire

  

## PARTIE 0: Mise en place

Voici les étapes pour installer imunes sur votre système:

-   Nous allons passer par une machine virtuelle dans laquelle imunes est déjà installé. Cette machine virtuelle est a télécharger ici [partage.rt-iut.re](http://partage.rt-iut.re/rtcloud/) depuis le réseau rt dans logiciel/reseaux/imnues/ et choisissez imunes-11.0-tah. C'est un fichier ova qui s'installe facilement en faisant dans virtualbox Fichier\Importer Un Appareil Virtuel.
    
-   Dans virtualbox, vous devez créer une interface de management. Dans Fichier\Host Network Manageret rajouter une interface si elle n'existe pas. Cette interface doit avoir un nom du genre vboxnet
    
-   mot de passe session imune
    
-   Avant de démarrer la machine imunes vérifier la configuration réseau/ram etc... pas besoin spécifiquement d'un accès par pont dans notre cas.
    
-   Lancer la machine virtuelle.
    
-   Pour changer la disposition du clavier lancer un terminal et lancer la commande: setxkbmap -layout fr
    

##### Premiers tests

-   Télécharger le squelette du réseau que nous allons utiliser. Il s'appelle [mydns2.imn](https://gitlab.com/M3105/tp/raw/master/files/mydns2.imn?inline=false). Sauvegarder le dans /root par exemple. C'est un fichier imunes que vous pouvez télécharger avec la commande wget https://gitlab.com/M3105/tp/raw/master/files/mydns2.imn depuis un terminal.
    
-   Lancer l'application imunes dans la machine virtuelle en double-cliquant sur l'icone imunes. Dans l'interface d'imunes cliquez sur file/open et ouvrez le fichier que vous venez de télécharger. Vous devriez obtenir la topologie suivante.
    

  
  
  
  
  
  
  
  
  
  
  
  

![](https://lh3.googleusercontent.com/ztwB5AHQewlz3q87T30mMNUoAcwT02x938g6tScHWx1vC65TLLnpq108KV7zxkIyvno0UVaNxr_Y_UAprtqVrDSkj8pXd_U2d7F5GJXIZRWBikN8gDKQ7MVRhYHzrNh-7woCDBJI)

Cette topologie est assez clair, mais nous reviendrons dans les détails plus tard dans ce TP si besoin.

Pour démarrer toutes les machines, et l'expérimentation dans la fenêtre imunes, cliquez sur Experiment/Execute. Les machines et les routeurs démarrent en quelques secondes.

-   Le routage est configuré avec le protocole RIP sur les routeurs
    
-   Sur chaque serveur ou PC vous avez un système freebsd qui tourne. En faisant un clic droit par exemple sur pc1 vous disposez de certaines options. Ce qui nous intéresse pour le moment: clic droit sur pc1 puis Shell window/bash pour obtenir un shell sur pc1. Les commandes classiques unix fonctionnent normalement (ifconfig, netstat, route).
    
-   Pour tester la topologie vous pouvez faire depuis le shell de pc1: ping 10.0.0.10 pour un ping vers aRootServer.
    
-     
    

  

        root@pc1:/ # ping 10.0.0.10  
        PING 10.0.0.10 (10.0.0.10): 56 data bytes  
        64 bytes from 10.0.0.10: icmp_seq=0 tt1=61 •ime=0.200 ms  
        64 bytes from 10.0.0.10: icmp_seq=1 tt1=61 time=0.273 ms  
        64 bytes from 10.0.0.10: icmp_seq=2 tt1=61 time=0.0.96 ms  
        ^C  
        --- 10.0.0.10 ping statistics ---  
    
    3 packets transmitted, 3 packets received, O.U. packet loss  
    round-trip min/avg/max/stddev = 0.0.96/0.1.90/0.273/0.073 ms

Astuce : pour ne pas avoir de capture, je vous conseille de faire une capture du résultat que vous souhaitez et ensuite à partir de l’image aller sur des site pour récupérer le texte à partir d’une image copier coller le résultat et voilà vous avez du texte

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

Bibliographe : [https://gitlab.com/M3105/tp/blob/master/TP2.md](https://gitlab.com/M3105/tp/blob/master/TP2.md) Partie 0

## PARTIE 1: La topologie

  

Le topologie est composé de:

  

-   Un serveur racine . à configurer. Normalement il y a en a 13 comme vu en cours, ici nous n'allons en configurer qu'un seul c'est le aRootServer
    

  

-   Il y a deux TLD (Top Level Domain) à configurer celui du .re et celui de .org sur les machines dre et dorg respectivement.
    
-   Il y a 2 AD (Authoritative Domain) à configurer wiki.org, et iut.re sur dwikiorg, et diutrerespectivement. Il y a un domaine en délégation rt.iut.re gérer par le serveur drtiutre
    

  

-   Dans le domaine wiki.org il y a la machine www à configurer et à inclure dans le domaine.
    
-   Dans le domaine rt.iut.re il y a les machines www et pc2 à configurer et inclure dans le domaine.
    
-   Dans le domaine iut.re il y a les machines ww wet pc1 à configurer et inclure dans le domaine.
    
-   Dans chacun des domaines, vous mettrez aussi une gateway.
    

  

Ce qu'il faut savoir:

bind est déjà installé sur les machines. Je ne rentre pas dans le processus d'installation sur freebsd qui est aussi simple que sous linux.

En revanche il n'y a pas de fichiers d'exemples, tous les fichiers sont à créer. (les fichiers de zones, named.conf etc ...)

Pour démarrer le serveur dns la commande est named. Pour éteindre vous pouvez faire un killall -9  named. La commande named-checkconf est disponible. Faites un man sur named pour plus d'infos.

Le fichier named.conf doit se trouver dans /etc. Si vous le créer à un autre endroit vous pouvez lancer la commande named -c /chemin/vers/named.conf

## PARTIE 2: Serveur d'autorité

Créer le répertoire `/etc/named` qui va contenir les configuration des zones avec `mkdir -p /etc/named`

Editer le fichier `/etc/named.conf` (vous devez le créer si besoin).

  

    options {  
    directory "/etc/named";  
    };  
      
    zone "." {  
    type hint;  
    file "named.root";  
    };  
      
    zone "0.0.127.IN-ADDR.ARPA" {  
    type master;  
    file "localhost.rev";  
    };  
      
    zone "wiki.org" {  
    type master;  
    file "wiki.dir";  
    };  
      
    zone "7.0.10.IN-ADDR.ARPA" {  
    type master;  
    file "wiki.rev";  
    };

Tester si votre fichier  `/etc/named.conf` est correct avec la commande `named-checkconf /etc/named.conf`

Si la commande ne retourne rien c’est que votre fichier est bien configurer

Il y a 4 zones que vous devez créer ici: ., 0.0.127.IN-ADDR.ARPA, wiki.org et 7.0.10.IN-ADDR.ARPA. Vous devez créer les fichiers correspondant : `/etc/named/named.root`, /`etc/named/localhost.rev, /etc/named/wiki.dir et /etc/named/wiki.rev.`

le fichier `/etc/named/named.root` contient l'adresse du serveur racine.

  
  
  
  

    $TTL  60000  
    @ IN  SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org. (  
    2002102801  ; serial  
    28  ; refresh  
    14  ; retry  
    3600000  ; expire  
    0  ; default_ttl  
    )  
      
    . 3600000  IN  NS aRootServer.  
    aRootServer. 3600000  A  10.0.0.10

  
  

-   Fichier /etc/named/localhost.rev
    

  
  

    $TTL  86400  
    @  IN  SOA  localhost. root.localhost (  
    20041128  ; Serial  
    28800  ; Refresh  
    7200  ; Retry  
    3600000  ; Expire  
    86400  ; Minimum  
    )  
    IN  NS  localhost.  
    1  IN  PTR  localhost.

  

-   Fichier /etc/named/wiki.dir
    

  
  

    $TTL  60000  
    @ IN  SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (  
    2002102801  ; serial  
    28  ; refresh  
    14  ; retry  
    3600000  ; expire  
    0  ; default_ttl  
    )  
    wiki.org. IN  NS  dwikiorg.wiki.org.  
    wiki.org. IN  A  10.0.7.10  
    dwikiorg IN  A  10.0.7.10  
    www IN  A  10.0.7.11  
    gateway IN  A  10.0.7.1

  

-   Fichier /etc/named/wiki.rev
    

  

    $TTL  60000  
    @ IN  SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (  
    2002102801  ; serial  
    28  ; refresh  
    14  ; retry  
    3600000  ; expire  
    0  ; default_ttl  
    )  
    @  IN  NS  dwikiorg.wiki.org.  
    1  IN  PTR  gateway.wiki.org.  
    10  IN  PTR  dwikiorg.wiki.org.  
    11  IN  PTR  www.wiki.org.

  
  
  

Pour la Partie rt.iut.re

    $TTL  60000  
    @ IN  SOA diutre.iut.re. root.diutre.iut.re. (  
    2002102801  ; serial  
    28  ; refresh  
    14 ; retry  
    3600000  ; expire  
    0  ; default_ttl  
    )  
    . 3600000  IN  NS aRootServer.  
    aRootServer. 3600000  A  10.0.0.10

  
  
  
  

Testez chaque fichier avec la commande `named-checkconf`

## PARTIE 3: Top Level Domain

Une fois les authoritative configurés, il faut configurer les TLD, dans notre cas .re et .org. Configuration guidée. Nous allons procédé avec le domaine .org dont le serveur est dorg

-   Ce serveur à l'adresse IP 10.0.2.10/24, cette adresse est déjà affecté en statique.
    
-   Pas besoin d'éditer le fichier /etc/host et le hostname est déjà configuré correctement.
    
-   Créer le répertoire /etc/named qui va contenir les configuration des zones.
    
-   Editer le fichier /etc/named.conf (vous devez le créer si besoin).
    

  

    options {  
    directory "/etc/named";  
    };  
      
    zone "." {  
    type hint;  
    file "named.root";  
    };  
      
    zone "org" {  
    type master;  
    file "org";  
    };  
      
    zone "0.0.127.IN-ADDR.ARPA" {  
    type master;  
    file "localhost.rev";  
    };

  

ici le type hint signifie que l'on donne au serveur dns une liste de serveur racine.

-   Tester si votre fichier `/etc/named.conf` est correct avec la commande `named-checkconf /etc/named.conf`
    
-   Il y a 3 zones que vous devez créer ici: ., 0.0.127.IN-ADDR.ARPA et org. Vous devez créer les fichiers correspondant : /etc/named/named.root, /etc/named/localhost.rev et /etc/named/org.
    
-   Le fichier /etc/named/named.root est sensiblement le même que celui de l'authoritative car il référence simplement les serveurs racines. (voir plus haut).
    
-   Le fichier /etc/named/localhost.rev est lui aussi sensiblement le même que le précédent.
    
-   Le fichier /etc/named/org
    

  

-   On crée et édite aussi le fichier /etc/named.conf :
    

    options {  
    directory "/etc/named";  
    };  
    zone "." {  
    type hint;  
    file "named.root";  
    };  
    zone "org" {  
    type master;  
    file "org";  
    };  
    zone "0.0.127.IN-ADDR.ARPA" {  
    type master;  
    file "localhost.rev";  
    };

Le fichier /etc/named/named.root est sensiblement le même que celui des authoritatives car il

référence simplement les serveurs racines.

Le fichier /etc/named/localhost.rev est lui aussi sensiblement le même

-   Fichier /etc/named/org :
    

    $TTL  60000  
    @ IN  SOA dorg.org. root.dorg.org (  
    2002102801  ; serial  
    28 ; refresh  
    14 ; retry  
    3600000  ; expire  
    0  ; default_ttl  
    )  
    @ IN  NS dorg.org.  
    dorg.org. IN  A  10.0.2.10  
    wiki.org. IN  NS dwikiorg.wiki.org.

  

  

  

-   Fichier /etc/named.conf :
    

>     options {  
>     directory "/etc/named";  
>     };  
>     zone "." {  
>     type hint;  
>     file "named.root";  
>     };  
>     zone "re" {  
>     type master;  
>     file "re";  
>     };  
>     zone "0.0.127.IN-ADDR.ARPA" {  
>     type master;  
>     file "localhost.rev";  
>     };

  

-   Fichier /etc/named/re :
    

$TTL  60000  
@ IN  SOA dre.re. root.dre.re (  
2002102801  ; serial  
28  ; refresh  
14 ; retry  
3600000  ; expire  
0 ; default_ttl  
)  
@ IN  NS dre.re.  
dre.re. IN  A  10.0.3.10  
iut.re. IN  NS diutre.iut.re.  
diutre.iut.re. IN  A  10.0.5.10  
rt.iut.re. IN  NS drtiutre.rt.iut.re.  
drtiutre.rt.iut.re. IN  A  10.0.6.10

  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

## PARTIE 4: RootServer

Configuration guidée du serveur racine aRootServer.

-   Ce serveur à l'adresse IP 10.0.0.10/24, cette adresse est déjà affecté en statique.
    
-   Pas besoin d'éditer le fichier /etc/host et le hostname est déjà configuré correctement.
    
-   Créer le répertoire /etc/named qui va contenir les configuration des zones.
    
-   Editer le fichier /etc/named.conf (vous devez le créer si besoin).
    

    options {  
    directory "/etc/named";  
    };  
      
    zone "." {  
    type master;  
    file "root";  
    };  
      
    zone "0.0.127.IN-ADDR.ARPA" {  
    type master;  
    file "localhost.rev";  
    };  
      
    zone "IN-ADDR.ARPA" {  
    type master;  
    file "in-addr.arpa";  
    };  
      
    
      

Tester si votre fichier /etc/named.conf est correct avec la commande named-checkconf /etc/named.conf

La commande ne retourne aucun résultat, le fichier est correct

-   Il y a 3 zones que vous devez créer ici: ., 0.0.127.IN-ADDR.ARPA et IN-ADDR.ARPA. Vous devez créer les fichiers correspondant : /etc/named/root, /etc/named/localhost.rev et /etc/named/in-addr.arpa.  
      
    
-   le fichier /etc/named/root
    

    $TTL  60000  
    @ IN  SOA aRootServer. root.aRootServer (  
    2002102801  ; serial  
    28800  ; refresh  
    14400  ; retry  
    3600000  ; expire  
    0  ; default_ttl  
    )  
      
    @ IN  NS aRootServer.  
    aRootServer. IN  A  10.0.0.10  
      
    org. IN  NS dorg.org.  
    dorg.org. IN  A  10.0.2.10  
      
    re. IN  NS dre.re.  
    dre.re. IN  A  10.0.3.10  
      

  
  

## PARTIE 5: Aller plus loin

  
  
  
  
  
  
  
  

## troubleshooting

Mon PC je ne sais pas pourquoi la machine virtuel lague un peu

Un peu de mal à prendre en main le script de déploiement

  

## Script de déploiement

## Exemple

    La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg  
    himage dwikiorg mkdir -p /etc/named  
      
    La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/  
    hcp wiki.org/named.conf dwikiorg:/etc/.  
      
    #La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire /etc/named sur la machine dwikiorg  
    hcp wiki.org/* dwikiorg:/etc/named/.  
      
     la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf  se trouvant sur la machine dwikiorg  
    himage dwikiorg rm /etc/named/named.conf

  
  

Pour le lancer faire simple ./deploy.sh

vérifier que vous avez tout les droit sur le fichier avant






